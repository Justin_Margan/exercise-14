﻿using System;

namespace exercise_14
{
    class Program
    {
        static void Main(string[] args)
        {
         //Start the program with Clear();
         Console.Clear();
         
         Console.WriteLine("What month where you born in?");
         Console.ReadLine();
         
         //End the program with blank line and instructions
         Console.ResetColor();
         Console.WriteLine();
         Console.WriteLine("Press <Enter> to quit the program");
         Console.ReadKey();
            
        }
    }
}
